// +build args_base64_sops

package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"os"
	"os/exec"
	"strings"
)

func init() {
	parsers = append(parsers, func(args []string) []string {
		newArgs := make([]string, len(args))
		for i, v := range args {
			var data struct {
				Sops interface{} `json:"sops"`
			}
			equal := strings.Index(v, "=")
			if equal == -1 {
				newArgs[i] = args[i]
				continue
			}
			value, err := base64.StdEncoding.DecodeString(v[equal+1:])
			if err != nil {
				newArgs[i] = args[i]
				continue
			}
			if err := json.Unmarshal(value, &data); err != nil || data.Sops == nil {
				newArgs[i] = args[i]
				continue
			}
			var out bytes.Buffer
			cmd := exec.Command("sops", "-d", "/dev/stdin")
			cmd.Stdin = bytes.NewReader(value)
			cmd.Stdout = &out
			cmd.Stderr = os.Stderr
			if err := cmd.Run(); err != nil {
				os.Exit(1)
			}
			newArgs[i] = v[0:equal] + "=" + out.String()
		}
		return newArgs
	})
}
