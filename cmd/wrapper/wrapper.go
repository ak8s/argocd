package main

import (
	"os"
	"os/exec"
)

func main() {
	cmdName := "_" + os.Args[0]
	newArgs := os.Args[1:]
	for _, p := range parsers {
		newArgs = p(newArgs)
	}
	cmd := exec.Command(cmdName, newArgs...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			os.Exit(exitError.ExitCode())
		}
		os.Exit(1)
	}
}
