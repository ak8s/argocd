# ArgoCD
ArgoCD image with:
* cdk8s
* sops
* ksops
* doppler (to provide gpg key)
* custom helm wrapper which handles sops encoded files in base64 see (`cmd/wrapper`)

