#!/usr/bin/env bash

while test $# -gt 0; do
  case $1 in
  -n | --name)
    shift
    if test $# -gt 0; then
      export IMAGE_NAME=$1
    else
      echo "no version specified"
      exit 1
    fi
    shift
    ;;
  -a | --argocd-version)
    shift
    if test $# -gt 0; then
      export ARGOCD_VERSION=$1
    else
      echo "no argocd version specified"
      exit 1
    fi
    shift
    ;;
  -c | --cdk8s-version)
    shift
    if test $# -gt 0; then
      export CDK8S_VERSION=$1
    else
      echo "no cdk8s version specified"
      exit 1
    fi
    shift
    ;;
  -k | --ksops-version)
    shift
    if test $# -gt 0; then
      export KSOPS_VERSION=$1
    else
      echo "no ksops version specified"
      exit 1
    fi
    shift
    ;;
  -s | --sops-version)
    shift
    if test $# -gt 0; then
      export SOPS_VERSION=$1
    else
      echo "no ksops version specified"
      exit 1
    fi
    shift
    ;;
  -p | --push)
    shift
    export PUSH_IMAGE='true'
    ;;
  --no-push)
    shift
    export PUSH_IMAGE='false'
    ;;
  -l | --latest)
    shift
    export TAG_LATEST='true'
    ;;
  -r | --revision)
    shift
    if test $# -gt 0; then
      export REVISION=$1
    else
      echo "no revision specified"
      exit 1
    fi
    shift
    ;;
  esac
done

IMAGE_NAME=${IMAGE_NAME:-'registry.gitlab.com/ak8s/argocd'}
ARGOCD_VERSION=${ARGOCD_VERSION:-'v1.7.9'}
CDK8S_VERSION=${CDK8S_VERSION:-'v0.33.0'}
KSOPS_VERSION=${KSOPS_VERSION:-'v2.2.2'}
SOPS_VERSION=${SOPS_VERSION:-'v3.6.1'}
PUSH_IMAGE=${PUSH_IMAGE:-'false'}
TAG_LATEST=${TAG_LATEST:-'true'}
REVISION=${REVISION:-''}

if [[ -z "$REVISION" ]]; then
  version="$ARGOCD_VERSION-cdk8s.${CDK8S_VERSION}-ksops.${KSOPS_VERSION}-sops.${SOPS_VERSION}"
else
  version="$ARGOCD_VERSION-$REVISION-cdk8s.${CDK8S_VERSION}-ksops.${KSOPS_VERSION}-sops.${SOPS_VERSION}"
fi

echo "[name]: ${IMAGE_NAME}"
if [ "${TAG_LATEST}" == "true" ]; then
  echo "[tags]: ${version}, latest"
else
  echo "[tag]: ${version}"
fi
echo "[push] ${PUSH_IMAGE}"

docker build -t "${IMAGE_NAME}:${version}" . \
  --build-arg ARGOCD_VERSION="${ARGOCD_VERSION}" \
  --build-arg CDK8S_VERSION="${CDK8S_VERSION}" \
  --build-arg KSOPS_VERSION="${KSOPS_VERSION}" \
  --build-arg SOPS_VERSION="${SOPS_VERSION}"

if [ "${TAG_LATEST}" == "true" ]; then
  docker tag "${IMAGE_NAME}:${version}" "${IMAGE_NAME}:latest"
fi

if [ "${PUSH_IMAGE}" == "true" ]; then
  docker push "${IMAGE_NAME}:${version}"
  if [ "${TAG_LATEST}" == "true" ]; then
    docker push "${IMAGE_NAME}:latest"
  fi
fi
