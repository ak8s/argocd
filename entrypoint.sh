#!/usr/bin/env bash

if [[ -n "$DOPPLER_TOKEN" ]]; then
  doppler run -- printenv GPG_PUBLIC_KEY | gpg --import
  doppler run -- printenv GPG_PRIVATE_KEY | gpg --import
fi

exec "$@"