ARG KSOPS_VERSION
ARG ARGOCD_VERSION

FROM viaductoss/ksops:$KSOPS_VERSION as ksops-builder

FROM golang:1.15 as wrapper-builder

ENV CGO_ENABLED=0
WORKDIR /go/src
COPY ./go.mod .
COPY ./cmd ./cmd
RUN go build -o /go/bin/wrapper -ldflags '-w' ./cmd/wrapper \
    && go build -o /go/bin/wrapper_args_base64_sops -ldflags '-w' ./cmd/wrapper

FROM argoproj/argocd:$ARGOCD_VERSION

ARG CDK8S_VERSION
ARG SOPS_VERSION

USER root

# setup kustomize
ENV XDG_CONFIG_HOME=$HOME/.config
ENV KUSTOMIZE_PLUGIN_PATH=$XDG_CONFIG_HOME/kustomize/plugin/
COPY --from=ksops-builder /go/bin/kustomize /usr/local/bin/_kustomize
COPY --from=wrapper-builder /go/bin/wrapper /usr/local/bin/kustomize
COPY --from=ksops-builder /go/src/github.com/viaduct-ai/kustomize-sops/*  $KUSTOMIZE_PLUGIN_PATH/viaduct.ai/v1/ksops/

# setup helm
RUN mv /usr/local/bin/helm /usr/local/bin/_helm
COPY --from=wrapper-builder /go/bin/wrapper_args_base64_sops /usr/local/bin/helm

# setup cdk8s
RUN set -x; \
    apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        gpg \
        rsync \
    && (curl -sL https://deb.nodesource.com/setup_lts.x) | bash - \
    && apt-get install -y nodejs \
    && (curl -L https://raw.githubusercontent.com/pnpm/self-installer/master/install.js) | node \
    && ln -f -s $(which pnpm) $(which npm) \
    && pnpm install -g cdk8s-cli@$CDK8S_VERSION \
    && mkdir -p /tmp/example \
    && (cd /tmp/example && cdk8s init typescript-app) \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# setup doppler
RUN (curl -Ls https://cli.doppler.com/install.sh || wget -qO- https://cli.doppler.com/install.sh) | sh

# setup sops
RUN set -x; \
    curl -o /usr/local/bin/sops -L https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux \
    && chmod +x /usr/local/bin/sops

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

USER argocd